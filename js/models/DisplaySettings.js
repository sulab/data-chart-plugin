define(['backbone', 'extensions/Factors', 'extensions/Sort',
        'extensions/Search'], 
        function(Backbone, factors, sort,
                 search){
  'use strict';

  return Backbone.RelationalModel.extend({
    defaults: {
      zoom : { low: 0.0, high: 1.0 },
    },
    initialize : function() {
      if ( this.isNew() ) {
        var display = factors.getDisplayObject(this.get('parentDataset'));
        this.set('colors',    display.colors,     {silent: true});
        this.set('factors',   display.factors,    {silent: true});

        this.set('order',     display.order,      {silent: true});
        this.set('shown',     display.shown,      {silent: true});
        this.set('aggregate', display.aggregate,  {silent: true});
      }

      this.get('parentDataset').get('viz').display = this.toJSON();

      //-- Setup the event listeners
      this.bind('change:zoom',      this.updateZoom);
      this.bind('change:order',     this.updateOrder);
      this.bind('change:colors',    this.updateColors);
      this.bind('change:shown',     this.updateShown);
      this.bind('change:aggregate', this.updateAggregate);
    },

    updateZoom : function() {
      // console.log('DisplaySettings', 'updateZoom');
      var viz = this.get('parentDataset').get('viz');
      if(viz.$el.length) {
        viz.display = this.toJSON();
        viz.changeZoom();
      };
    },

    updateOrder : function() {
      console.log('DisplaySettings', 'updateOrder');
      var data = this.get('parentDataset').get('cached_data');
      var aggregate_on = sort.aggregate_on(this);

      if( aggregate_on.length ) {
        var filters = sort.cartesianOdometer(aggregate_on, this);
        data = search.dataset(filters, aggregate_on, data, true);
      }

      //-- After we aggreated, sort them all still (sample n will be
      //-- less than total samples due to agg)
      // data = sort.byFactor(this);
      this.get('parentDataset').set('data', data); 

      var viz = this.get('parentDataset').get('viz');
      viz.display = this.toJSON();
      viz.refresh();
    },

    updateShown : function() {
      console.log('DisplaySettings', "updateShown");
    },

    updateAggregate : function() {
      console.log('DisplaySettings', "updateAggregate");
    },

    updateColors : function() {
      console.log('DisplaySettings', 'updateColors');
    },

  });
});
