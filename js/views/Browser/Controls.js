define(['marionette', 'templates', 'vent'], 
      function (Marionette, templates, vent) {
  'use strict';

  return Marionette.ItemView.extend({
    template : templates.browser.controls,
    templateHelpers : function() {
      return this.collection;
    },

    events : {
      'click a.serverprevious' : 'previousResultPage',
      'click a.servernext'     : 'nextResultPage',

      'click a.serverfirst'    : 'gotoFirst',
      'click a.serverlast'     : 'gotoLast',

      'click a.page'           : 'gotoPage',
      'click a.serverpage'     : 'gotoPage',

      'change #datasetSearch'  : 'gotoFirst',
      // 'change #defaultDSToggle input' : 'toggleDefault',
    },

    //-- Stnd View Funcs
    initialize: function(options) {
      // this.collection.on('reset', this.render, this);
      // this.collection.on('change', this.render, this);
    },

    //-- View event triggers
    nextResultPage : function(evt) {
      evt.preventDefault();
      this.collection.requestNextPage();
    },
    previousResultPage : function(evt) {
      evt.preventDefault();
      this.collection.requestPreviousPage();
    },
    gotoFirst : function(evt) {
      evt.preventDefault();
      this.collection.goTo(this.collection.firstPage);
    },
    gotoLast : function(evt) {
      evt.preventDefault();
      this.collection.goTo(this.collection.lastPage);
    },
    gotoPage : function(evt) {
      evt.preventDefault();
      var page = $(evt.target).text();
      this.collection.goTo(page);
    },

    // toggleDefault : function(evt) {
    //   app.state.toggleDefault = $(evt.target).is(':checked');
    //   $('ul#defaultList').slideToggle();
    // },

  });
});
