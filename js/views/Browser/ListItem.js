define(['marionette', 'templates', 'vent'], 
      function (Marionette, templates, vent) {
  'use strict';

  return Marionette.ItemView.extend({
    template : templates.browser.item,
    tagName : 'li',

    events : {
      'click .image' : 'clickArrow',
      'click p'      : 'clickArrow',
      'click .block' : 'clickArrow'
    },

    initialize : function() {
      // this.model.bind('change', this.render, this);
      this.model.bind('destroy', this.remove, this);
    },

    clickArrow : function(evt) {
      Backbone.history.navigate('/'+ app.state.entrez_id +'/'+ this.model.get('id'), {trigger: true});
    },

  });
});
