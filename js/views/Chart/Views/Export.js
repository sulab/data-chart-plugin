define(['marionette', 'templates', 'vent',
        'extensions/Utils', 'ZeroClipboard'], 
    function (Marionette, templates, vent,
              Utils, ZeroClipboard) {
  'use strict';

  window.ZeroClipboard = ZeroClipboard;
  return Marionette.ItemView.extend({
    template : templates.chart.views._export,

    ui : {
      exportarea : 'textarea.export',
      copy       : '#copy-paste-btn'
    },

    onRender : function() {
      var self = this;
      this.requestData();

      ZeroClipboard.setDefaults({
        moviePath: '/flash/ZeroClipboard.swf',
      });

      this.clip = new ZeroClipboard( this.ui.copy );
      this.clip.addEventListener('mousedown', function(client) {
        self.clip.setText(self.ui.exportarea.val());
      });

    },

    returnCSV : function(tissues, results) {
      var self = this,
          csv = 'Tissue,'+ self.model.get('probesets').join(',');
      _.each(tissues, function(row, index) {
        csv += "\n";
        var arr = [];
        _.each(self.model.get('probesets'), function(ps) {
          arr.push( +results[ps][index] ) 
        });
        csv += ( tissues[index]+','+arr.join(',') );
      });

      this.ui.exportarea.val( csv );
    },

    requestData: function() {
      var self = this,
          results = {},
          count = 0,
          tissues;

      _.each(self.model.get('probesets'), function(ps, i) {
        $.ajax({
          type: 'GET',
          url: Utils.proxy( 'dataset/d3/'+ self.model.get('dataset_id') +'/'+ ps +'/?format=json&callback=?' ),
          dataType: 'json',
          success: function(d) {
            results[d.meta.reporter] = _.pluck(d.data, 'value');
            if(i === 0) {
              tissues = _.pluck(d.data, 'title');
            }
            count += 1;
            if(count === self.model.get('probesets').length) {
              self.returnCSV(tissues, results);
            }
          },
          async: false,
        });
      })
    },

  });
});
