define(['marionette', 'templates', 'vent'], 
    function (Marionette, templates, vent) {
  'use strict';

  return Marionette.ItemView.extend({
    template : templates.chart.modal.options_list_item,
    tagName   : 'tr',
    className : function() { return this.model.get('factor'); },

    onRender : function() {
      this.$el.find('.color_background').css({'background-color': this.model.get('color') });
    }

  });
});
