define(['marionette', 'templates', 'vent',
        'bootstrap'], 
        function (Marionette, templates, vent) {
  'use strict';

  return Marionette.ItemView.extend({
    template : templates.chart.footer,
    templateHelpers : function() {
      return {
        selectList : this.selectList
      }
    },

    ui : {
      select : 'select#navigation',
      link   : 'ul.dropdown-menu li a'
    },

    events : {
      'click ul.dropdown-menu li a' : 'toggleViewType'
    },

    toggleViewType : function(evt) {
      evt.preventDefault();
      vent.trigger('chart:toggle', $(evt.target).html().trim().toLowerCase() );
    },

    selectList : ['datachart', 'image', 'export']

  });
});
