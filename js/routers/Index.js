define(['marionette'],
    function(marionette) {
  'use strict';

  return marionette.AppRouter.extend({
    appRoutes:{
      ':entrez_id'             : 'showDefault',
      ':entrez_id/'            : 'showDefault',
      ':entrez_id/:dataset_id' : 'showDefault',

      '*actions'   : 'wildcardRoute',
    }

  });
});
