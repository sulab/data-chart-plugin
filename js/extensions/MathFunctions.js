/*global define*/

define([], 
    function () {
  'use strict';

  return {
    getMean : function(numArr) {
      var i = numArr.length,
          sum = 0;
      while(i--) { sum += numArr[ i ]; };
      return sum / numArr.length;
    },

    getVariance : function(numArr) {
      var mean = this.getMean(numArr),
          i = numArr.length,
          v = 0;
      while( i-- ) { v += Math.pow( (numArr[ i ] - mean), 2 ); };
      return v / numArr.length;
    },

    getStandardDeviation : function(numArr) {
      return Math.sqrt( this.getVariance(numArr) );
    },

    getStandardError : function(numArr) {
      return this.getStandardDeviation(numArr) / Math.sqrt(numArr.length);
    },
  }
});
