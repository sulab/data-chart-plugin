define([], 
    function () {
  'use strict';

  return {
    getOrder : function(sample_factors) {
      var self = this;
      //-- sample_factors is the freshly plucked array of
      //-- factor objects taken straight from the d3 data API
      var factors_arr = [];
      _.each(sample_factors, function(val) { factors_arr.push( _.map(_.keys(val), function(item) { return _.str.slugify(item) }) ); })
      //-- This "order" dictates the left<->right position of factor types
      return _.without( _.intersection.apply(_, factors_arr), 'title');
    },

    getOptions : function(factor, sample_factors) {
      var options = _.uniq( _.pluck(sample_factors, factor) );
      //-- This "order" dictates the top<->bottom position of the factor
      //-- options
      return options.sort(function(a,b){return a-b;});
    },

    optionsNumeric : function(options) {
      options = _.map(options, function(item) { return (_.isNaN(+item) === true) ? item : +item; });
      return ( (_.filter(options, function(item) { return _.isNumber(item) === true }).length / options.length) >= 0.9) ? true : false;
    },

    getDisplayObject : function(model) {
      //-- Array of sorted factors, and hash of options for each factor
      var self = this,
          obj = {},
          val,
          isNumeric = false,
          sample_factors = _.pluck( model.get('cached_data'), 'factors');

      obj['order'] = self.getOrder( sample_factors );
      obj['factors'] = {};
      obj['colors'] = {};
      obj['shown'] = {};
      obj['aggregate'] = {};

      //-- Cleanup the sample_factors, can't be 100% positive these will
      //-- have hit the Modal validation for cached_dataset sourced arrs
      _.each(sample_factors, function(sample) {
        _.each(_.keys(sample), function(factor) {
          val = sample[ factor ];
          delete sample[ factor ];
          sample[ _.str.slugify(factor) ] = val;
        });
      });

      //-- Fill up the factor options and colors
      _.each(obj.order, function(factor) {
        //-- Assign the ordered list of factor options
        obj['factors'][factor] = self.getOptions( factor, sample_factors );
        obj['colors'][factor] = {};

        isNumeric = self.optionsNumeric( obj['factors'][factor] );
        if( isNumeric ) {
          var base = d3.hsl( model.get('factor_clr_scale')( model.get('clr_i') ) );
          model.set('clr_i', model.get('clr_i') + 1);
          var color = d3.scale.linear()
            .domain([ _.max(obj['factors'][factor]), _.min(obj['factors'][factor])])
            .range([ d3.hsl(base.h, base.s, 0.2) ,  d3.hsl(base.h, base.s, 0.8) ]);

          _.each( obj['factors'][factor], function(fct_option) {
            obj['colors'][factor][fct_option] = color( fct_option );
          });
        } else {
          //-- Assume these are just text options
          //-- Build a index object to assign each option a unique index so
          //-- that'll have a persistent color
         _.each( obj['factors'][factor] , function(fct_option) {
            obj['colors'][factor][fct_option] = model.get('factor_clr_scale')( model.get('clr_i') );
            model.set('clr_i', model.get('clr_i') + 1);
          });
        }

        obj['shown'][factor] = true;
        obj['aggregate'][factor] = false;
      });
      return obj;
    },

    validate : function() {
      var self = this,
          ST = app.state;
      _.each(ST.display.order, function(factor) {
        if(ST.display.factors[ factor ].length===1) {
          ST.display.order = _.without(ST.display.order, factor);
        }
      });
    }
  }
});
