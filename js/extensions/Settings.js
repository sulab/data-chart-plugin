/*global define*/

define([], 
    function () {
  'use strict';

  return {
    devMode : false,

    species : [
      {
        dataset_id    : 1,
        taxonomy_id   : 9606,
        common        : 'Human',
        genus         : 'Homo sapiens',
        display_order : -10,
      },
      {
        dataset_id    : 4,
        taxonomy_id   : 10090,
        common        : 'Mouse',
        genus         : 'Mus Musculus',
        display_order : -9,
      },
      {
        dataset_id    : 5,
        taxonomy_id   : 10116,
        common        : 'Rat',
        genus         : 'Rattus norvegicus',
        display_order : -8,
      },
      {
        dataset_id    : 0, //-- none
        taxonomy_id   : 7227,
        common        : 'Drosophila',
        genus         : 'Drosophila melanogaster',
        display_order : -7,
      },
      {
        dataset_id    : 0, //-- none
        taxonomy_id   : 6239,
        common        : 'C. Elegans',
        genus         : 'Caenorhabditis elegans',
        display_order : -6,
      },
      {
        dataset_id    : 0, //-- none
        taxonomy_id   : 7955,
        common        : 'Zebrafish',
        genus         : 'Danio rerio',
        display_order : -5,
      },
      {
        dataset_id    : 0, //-- none
        taxonomy_id   : 3702,
        common        : 'Arabidopsis',
        genus         : 'Arabidopsis thaliana',
        display_order : -4,
      },
      {
        dataset_id    : 0, //-- none
        taxonomy_id   : 8364,
        common        : 'Frog',
        genus         : 'Xenopus tropicalis',
        display_order : -3,
      },
      {
        dataset_id    : 2427,
        taxonomy_id   : 9823,
        common        : 'Pig',
        genus         : 'Sus scrofa',
        display_order : 0, //-- none
      }
    ]
  }
});
