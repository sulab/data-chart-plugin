define(['backbone', 'models/Dataset', 'backbone.localStorage'],
        function(Backbone, Dataset) {
  'use strict';

  return Backbone.Collection.extend({
    localStorage: new Backbone.LocalStorage("TSRI-DATACHART-PLUGIN"),
    model: Dataset,

    //-- Keep the collection in order of recent viewings
    comparator : function(component) { return -component.get('updated'); },
  });
});
