define(['backbone', 'models/DisplaySettings', 'backbone.localStorage'],
        function(Backbone, DisplaySettings) {
  'use strict';

  return Backbone.Collection.extend({
    localStorage: new Backbone.LocalStorage("TSRI-DATACHART-PLUGIN-DISPLAY-SETTINGS"),
    model: DisplaySettings,
  });
});
